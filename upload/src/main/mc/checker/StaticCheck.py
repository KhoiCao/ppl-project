#1710148
"""
 * @author nhphung
"""
from AST import * 
from Visitor import *
from Utils import Utils
from StaticError import *
from functools import reduce
from typing import List

class MType:
    def __init__(self,partype,rettype):
        self.partype = partype
        self.rettype = rettype

class Symbol:
    def __init__(self,name,mtype,value = None):
        self.name = name
        self.mtype = mtype
        self.value = value

class StaticChecker(BaseVisitor,Utils):

    global_envi = [
    Symbol("getInt",MType([],IntType())),
    Symbol("putIntLn",MType([IntType()],VoidType())),
    Symbol("putInt",MType([IntType()],VoidType())),
    Symbol("getFloat",MType([],FloatType())),
    Symbol("putFloat",MType([FloatType()],VoidType())),
    Symbol("putFloatLn",MType([FloatType()],VoidType())),
    Symbol("putBool",MType([BoolType()],VoidType())),
    Symbol("putBoolLn",MType([BoolType()],VoidType())),
    Symbol("putString",MType([StringType()],VoidType())),
    Symbol("putStringLn",MType([StringType()],VoidType())),
    Symbol("putLn",MType([],VoidType()))
    ]
        


    def __init__(self,ast):
        #print(ast)
        #print(ast)
        #print()
        self.ast = ast

 
    
    def check(self):
        return self.visit(self.ast,StaticChecker.global_envi)

    def visitProgram(self,ast, c): 
        self.in_loop = 0

        vardecl_list = list(filter(lambda x: type(x) is VarDecl, ast.decl))     #get all VarDecl
        funcdecl_list = list(filter(lambda x: type(x) is FuncDecl,ast.decl))    #get all FuncDecl

        #create list of Symbol for all FuncDecl to check undeclared function
        self.func_list = list(map(lambda x: Symbol(x.name.name,MType(list(map(lambda y: y.varType,x.param)),x.returnType)),funcdecl_list))
        if len(self.func_list) != 0:
            self.current_func = self.func_list[0]

        #create list of Symbol for all VarDecl to check undeclared identifier
        self.var_lst = list(map(lambda x: Symbol(x.variable,x.varType),vardecl_list))

        #create list of function name to check uncalled function
        self.uncalled_func = list(map(lambda x: x.name,self.func_list))
        #start visitting decls from top to bottom
        lst = reduce (lambda x,y: [x[0] + self.visit(y,x)],ast.decl,[c])

        if not list(filter(lambda x: x.name == "main",self.func_list)):     #look for main function
            raise NoEntryPoint()

        if len(self.uncalled_func) != 0:
            for x in self.uncalled_func:
                if x != "main":
                    raise UnreachableFunction(x)

        return "" 
        

    def visitFuncDecl(self,ast, c):
        if self.lookup(ast.name.name,c[0],lambda x: x.name) is None:
            try:
                local = reduce(lambda x,y: [x[0] + self.visit(y,x),c[0]],ast.param,[[],c[0]])
            except Redeclared as e:
                raise Redeclared(Parameter(),e.n)

            self.current_func = Symbol(ast.name.name,MType(list(map(lambda x: x.varType,ast.param)),ast.returnType))
            function_return = False

            for x in ast.body.member:
                if type(x) is VarDecl:
                    local = [ local[0] + self.visit(x,local),local[1] ]
                elif type(x) is Block or type(x) is If or type(x) is For or type(x) is Return or type(x) is Dowhile:   #new
                    function_return = function_return or self.visit(x,local)
                else:
                    self.visit(x,local)

            if type(ast.returnType) != VoidType:
                if function_return == False:
                    raise FunctionNotReturn(ast.name.name)

            return [self.current_func]
            
        else:
            raise Redeclared(Function(),ast.name.name)

    def visitVarDecl(self,ast,c):
        if self.lookup(ast.variable,c[0],lambda x: x.name) is None:
            return [Symbol(ast.variable,ast.varType)]
        else:
            raise Redeclared(Variable(),ast.variable)
    

    def visitCallExpr(self, ast, c): 

        if ast.method.name != self.current_func.name:      #if invoked function's name is different from the name of the function invoking it
            called = self.lookup(ast.method.name,self.uncalled_func,lambda x: x)    #look for the invoked function's name in uncalled-list
            if called is not None:      #if the name is in uncalled-list
                self.uncalled_func.remove(called)   #remove it from the list (becomes invoked now)

        res = self.lookup(ast.method.name,flatten(c[0:-1]) + StaticChecker.global_envi + self.func_list +self.var_lst,lambda x: x.name)
        if res is None:# or not type(res.mtype) is MType:
            raise Undeclared(Function(),ast.method.name)
        elif type(res.mtype) is not MType:
            raise TypeMismatchInExpression(ast)
        else:
            at = [self.visit(x,c) for x in ast.param]       #get param list
            if len(res.mtype.partype) != len(at):           #if length of formal param list is different from actual param list
                raise TypeMismatchInExpression(ast)
            else:
                for i in range(len(at)):                    #compare type of each param
                    if type(res.mtype.partype[i]) == ArrayPointerType and (type(at[i]) == ArrayPointerType or type(at[i]) == ArrayType):
                        if type(res.mtype.partype[i].eleType) == type(at[i].eleType):
                            continue
                        else:
                            raise TypeMismatchInExpression(ast)
                    else:
                        if type(res.mtype.partype[i]) != type(at[i]):
                            if type(res.mtype.partype[i]) == FloatType and type(at[i]) == IntType:
                                continue
                            raise TypeMismatchInExpression(ast)
        return res.mtype.rettype


    def visitBlock(self,ast,c):
        local = [[]] + c
        block_return = False    
        for x in ast.member:
            if type(x) is VarDecl:
                local = [ local[0] + self.visit(x,local) ] + c
            elif type(x) is Block or type(x) is If or type(x) is For or type(x) is Return or type(x) is Dowhile:   #new
                block_return = block_return or self.visit(x,local) 
            else:
                self.visit(x,local)
        return block_return     

    #checking TypeMismatchInExpression
    def visitUnaryOp(self,ast,c):
        body_type = self.visit(ast.body,c)
        if ast.op == "-":
            if type(body_type) is not IntType and type(body_type) is not FloatType:
                raise TypeMismatchInExpression(ast)
        else:
            if type(body_type) is not BoolType:
                raise TypeMismatchInExpression(ast)
        return body_type

    def visitBinaryOp(self,ast,c):
        if ast.op == "=":
            if type(ast.left) is not Id and type(ast.left) is not ArrayCell:
                raise NotLeftValue(ast.left)

        left_type = self.visit(ast.left,c)
        right_type = self.visit(ast.right,c)

        if ast.op == "=":
            if type(left_type) == VoidType or type(left_type) == ArrayType or type(left_type) == ArrayPointerType:
                raise TypeMismatchInExpression(ast)
            if type(left_type) is not type(right_type):
                if type(left_type) is FloatType and type(right_type) is IntType:
                    return left_type
                raise TypeMismatchInExpression(ast)
            return left_type

        if ast.op == "%":
            if type(left_type) != IntType or type(right_type) != IntType:
                raise TypeMismatchInExpression(ast)
            return left_type

        if ast.op in ["&&","||"]:
            if type(left_type) != BoolType or type(right_type) != BoolType:
                raise TypeMismatchInExpression(ast)
            return left_type

        if ast.op in ["==","!="]:
            if (type(left_type)==IntType and type(right_type)==IntType) or (type(left_type)==BoolType and type(right_type)==BoolType):
                return BoolType()
            else:
                raise TypeMismatchInExpression(ast)

        if ast.op in ["+","-","*","/"]:
            if (type(left_type) != IntType and type(left_type) != FloatType) or (type(right_type) != IntType and type(right_type) != FloatType):
                raise TypeMismatchInExpression(ast)
            elif type(left_type) == type(right_type):
                return left_type
            else:
                return FloatType()

        if ast.op in ["<",">","<=",">="]:
            if (type(left_type) != IntType and type(left_type) != FloatType) or (type(right_type) != IntType and type(right_type) != FloatType):
                raise TypeMismatchInExpression(ast)
            else:
                return BoolType()

    def visitArrayCell(self,ast,c):

        arr_type = self.visit(ast.arr,c)
        idx_type = self.visit(ast.idx,c)

        if (type(arr_type) != ArrayType and type(arr_type) != ArrayPointerType) or type(idx_type) != IntType:
            raise TypeMismatchInExpression(ast)
        else:
            return arr_type.eleType

    def visitId(self,ast,c):
        res = self.lookup(ast.name,flatten(c[0:-1]) + self.var_lst + self.func_list + StaticChecker.global_envi,lambda x: x.name)
        if res is None: 
            raise Undeclared(Identifier(),ast.name)
        else:
            return res.mtype

    #check TypeMismatchInStateMent

    def visitIf(self,ast,c):
        expr_type = self.visit(ast.expr,c)
        if type(expr_type) != BoolType:
            raise TypeMismatchInStatement(ast)
        then_return = False
        else_return = False
        if_return = False
        if type(ast.thenStmt) is Block or type(ast.thenStmt) is If or type(ast.thenStmt) is For or type(ast.thenStmt) is Return or type(ast.thenStmt) is Dowhile:
            then_return = self.visit(ast.thenStmt,c)
        else:
            self.visit(ast.thenStmt,c)
        if ast.elseStmt is not None:
            if type(ast.elseStmt) is Block or type(ast.elseStmt) is If or type(ast.elseStmt) is For or type(ast.elseStmt) is Return or type(ast.elseStmt) is Dowhile:
                else_return = self.visit(ast.elseStmt,c)
            else:
                self.visit(ast.elseStmt,c)
        if_return = then_return and else_return
        return if_return

    def visitDowhile(self,ast,c):
        self.in_loop = self.in_loop + 1
        dowhile_return = False
        for x in ast.sl:
            if type(x) is Block or type(x) is If or type(x) is For or type(x) is Return or type(x) is Dowhile:
                dowhile_return = dowhile_return or self.visit(x,c)
            else:
                self.visit(x,c)
        exp_type = self.visit(ast.exp,c)
        if type(exp_type) != BoolType:
            raise TypeMismatchInStatement(ast)
        self.in_loop = self.in_loop - 1
        return dowhile_return

    def visitFor(self,ast,c):
        self.in_loop = self.in_loop + 1
        for_return = False
        expr1_type = self.visit(ast.expr1,c)
        if type(expr1_type) != IntType:
            raise TypeMismatchInStatement(ast)
        expr2_type = self.visit(ast.expr2,c)
        if type(expr2_type) != BoolType:
            raise TypeMismatchInStatement(ast)
        expr3_type = self.visit(ast.expr3,c)
        if type(expr3_type) != IntType:
            raise TypeMismatchInStatement(ast)

        self.visit(ast.loop,c)
        self.in_loop = self.in_loop - 1
        return False

    def visitReturn(self,ast,c):

        if ast.expr is None:
            if type(self.current_func.mtype.rettype) != VoidType:
                raise TypeMismatchInStatement(ast)
        else:    
            if type(self.current_func.mtype.rettype) == VoidType:
                raise TypeMismatchInStatement(ast)
            expr_type = self.visit(ast.expr,c)
            if type(self.current_func.mtype.rettype)==ArrayPointerType and (type(expr_type)==ArrayPointerType or type(expr_type)==ArrayType):
                if type(self.current_func.mtype.rettype.eleType) == type(expr_type.eleType):
                    return True
                else:
                    raise TypeMismatchInStatement(ast)
            if type(expr_type) != type(self.current_func.mtype.rettype):
                if type(self.current_func.mtype.rettype) == FloatType and type(expr_type) == IntType:
                    return True
                else:
                    raise TypeMismatchInStatement(ast)
        return True

    def visitBreak(self,ast,c):
        if self.in_loop == 0:
            raise BreakNotInLoop()

    def visitContinue(self,ast,c):
        if self.in_loop == 0:
            raise ContinueNotInLoop()
        

    def visitIntLiteral(self,ast, c): 
        return IntType()
    
    def visitFloatLiteral(self,ast,c):
        return FloatType()

    def visitStringLiteral(self,ast,c):
        return StringType()

    def visitBooleanLiteral(self,ast,c):
        return BoolType()











def flatten(lst):
    flat_list = [item for sublist in lst for item in sublist]
    return flat_list

