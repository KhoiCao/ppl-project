//1710148
grammar MC;

@lexer::header {
from lexererr import *
}

@lexer::members {
def emit(self):
    tk = self.type
    if tk == self.UNCLOSE_STRING:       
        result = super().emit();
        raise UncloseString(result.text);
    elif tk == self.ILLEGAL_ESCAPE:
        result = super().emit();
        raise IllegalEscape(result.text);
    elif tk == self.ERROR_CHAR:
        result = super().emit();
        raise ErrorToken(result.text); 
    else:
        return super().emit();
}

options{
	language=Python3;
}

/* 
 * Parser 
 */
program: manydecls EOF;
manydecls: decl+;
decl: vardecl | funcdecl;
vardecl: primtype var (CM var)* SM;
primtype: BOOLTYPE | INTTYPE | FLOATTYPE | STRINGTYPE;
var: ID | ID LSB INTLIT RSB;
funcdecl: types ID LB paramlist RB blockstmt;
types: primtype | arrptype | VOIDTYPE;
paramlist: (paramdecl (CM paramdecl)*)? ;
paramdecl: primtype ID | primtype ID LSB RSB;
arrptype: primtype LSB RSB;
ifstmt: ifnoelsestmt | ifelsestmt;
ifnoelsestmt: IF LB exp RB stmt;
ifelsestmt: IF LB exp RB stmt ELSE stmt;
dowhilestmt: DO stmtlist WHILE exp SM;
stmtlist: stmt+;
forstmt: FOR LB exp SM exp SM exp RB stmt;
breakstmt: BREAK SM;
continuestmt: CONT SM;
returnstmt: RETURN SM | RETURN exp SM;
expstmt: exp SM;
blockstmt: LP vardecl_stmt_list RP;
vardecl_stmt_list: vardecl_stmt*;
vardecl_stmt: vardecl | stmt;
stmt: ifstmt | dowhilestmt | forstmt | breakstmt | continuestmt | returnstmt | expstmt | blockstmt;

exp: (exp1 ASSIGN)* exp1;
exp1: exp2 (OR exp2)* ;
exp2: exp3 (AND exp3)* ;
exp3: exp4 (EQ | NEQ) exp4 | exp4;
exp4: exp5 (GT | LT | GTE | LTE) exp5 | exp5;
exp5: exp6 ( (ADD | SUB) exp6)* ;
exp6: exp7 ( (DIV | MUL | MOD) exp7)* ;
exp7: (SUB | NOT) exp7 | arrele;
arrele: exp8 LSB exp RSB | exp8;
exp8: INTLIT | STRINGLIT | FLOATLIT | BOOLEANLIT | ID | subexp | funcall;
funcall: ID LB explist RB;
explist: (exp (CM exp)*)?;
subexp: LB exp RB;






/* 
 *Lexer 
 */


INTLIT: DIGIT+;

 fragment DIGIT: [0-9];
 fragment EXPONENT: ('e'|'E') ('-')? DIGIT+ ;
 fragment DOT: '.';

FLOATLIT: DIGIT+ DOT DIGIT* EXPONENT? 
        | DIGIT* DOT DIGIT+ EXPONENT? 
        | DIGIT+ DOT? DIGIT* EXPONENT;

STRINGLIT: '"' ( '\\' [btnfr"\\] | ~[\t\f\r\n\\"] )* '"' {
            start = 1;
            end = len(self.text) - 1;
            step = None;
            self.text = self.text[start:end:step];
        }
;                            //  "(\\.|[^\\"])*";

BOOLEANLIT: TRUE | FALSE;


INTTYPE:'int';

VOIDTYPE: 'void' ;

FLOATTYPE: 'float';

BOOLTYPE: 'boolean';

STRINGTYPE: 'string';



BREAK: 'break';

CONT: 'continue';

ELSE: 'else';

FOR: 'for';

IF: 'if';

RETURN: 'return';

DO: 'do';

WHILE: 'while';

TRUE: 'true';

FALSE: 'false';

ID: [_a-zA-Z]+[_a-zA-Z0-9]* ;

LB: '(' ;

RB: ')' ;

LP: '{';

RP: '}';

LSB: '[';

RSB: ']';

SM: ';' ;

CM: ',' ;


ADD: '+';

SUB: '-';

MUL: '*';

DIV: '/';

MOD: '%';

NOT: '!';

OR: '||';

AND: '&&';

EQ: '==';

NEQ: '!=';

LTE: '<=';

GTE: '>=';

LT: '<';

GT: '>';

ASSIGN: '=';



LINE_CMT: '//' ~[\r\n]* -> skip ;

BLOCK_CMT: '/*' .*? '*/' -> skip;

WS : [ \t\r\n\b\f]+ -> skip ; // skip spaces, tabs, newlines


ERROR_CHAR: .{ raise ErrorToken(self.text); };
UNCLOSE_STRING: '"' ( '\\' [btnfr"\\] | ~[\t\f\r\n\\"] )* {
            start = 1
            end = len(self.text)
            step = None
            self.text = self.text[start:end:step]
};
ILLEGAL_ESCAPE: '"' ( '\\' ~[btnfr"\\] | ~[\t\f\r\n\\"] | '\\' [btnfr"\\])* '"'? {
    i = 0
    while i <= len(self.text):
        if self.text[i] == '\\':
            dest = i + 1
            if self.text[dest] == '\\':
                i = i + 2
                continue
            if self.text[dest] != 'b' and self.text[dest] != 't' and self.text[dest] != 'n' and self.text[dest] != 'f' and self.text[dest] != 'r' and self.text[dest] != '\"':
                break
        i = i + 1
    self.text = self.text[1:dest+1]
};