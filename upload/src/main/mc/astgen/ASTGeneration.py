#1710148
from MCVisitor import MCVisitor
from MCParser import MCParser
from AST import *
import functools
from functools import reduce
class ASTGeneration(MCVisitor):
    def visitProgram(self,ctx:MCParser.ProgramContext):
        return Program(self.visit(ctx.manydecls()))

    def visitManydecls(self,ctx:MCParser.ManydeclsContext):
        result = [self.visit(x) for x in ctx.decl()]
        flat_result = flatten(result)
        return flat_result

    def visitDecl(self,ctx:MCParser.DeclContext):
        if ctx.vardecl():
            return self.visit(ctx.vardecl())
        else:
            return self.visit(ctx.funcdecl())

    def visitVardecl(self,ctx:MCParser.VardeclContext):
        return [VarDecl( self.visit(x),ArrayType( int(x.INTLIT().getText()), self.visit(ctx.primtype())) ) if x.INTLIT() else VarDecl(self.visit(x),self.visit(ctx.primtype())) for x in ctx.var()] 

    def visitPrimtype(self,ctx:MCParser.PrimtypeContext):
        if ctx.INTTYPE():
            return IntType()
        if ctx.FLOATTYPE():
            return FloatType()
        if ctx.BOOLTYPE():
            return BoolType()
        else:
            return StringType()

    def visitVar(self,ctx:MCParser.VarContext):
        return ctx.ID().getText()

    def visitFuncdecl(self,ctx:MCParser.FuncdeclContext):
        return FuncDecl( Id(ctx.ID().getText()), self.visit(ctx.paramlist()) ,self.visit(ctx.types()), self.visit(ctx.blockstmt()))

    def visitTypes(self,ctx:MCParser.TypesContext):
        if ctx.VOIDTYPE():
            return VoidType()
        else:
            return self.visit(ctx.getChild(0))

    def visitArrptype(self,ctx:MCParser.ArrptypeContext):
        return ArrayPointerType(self.visit(ctx.primtype()))

    def visitParamlist(self,ctx:MCParser.ParamlistContext):
        return [self.visit(x) for x in ctx.paramdecl()]

    def visitParamdecl(self,ctx:MCParser.ParamdeclContext):
        if ctx.getChildCount() == 2:
            return VarDecl(ctx.ID().getText(),self.visit(ctx.primtype()))
        else:
            return VarDecl(ctx.ID().getText(),ArrayPointerType(self.visit(ctx.primtype())))

    def visitBlockstmt(self,ctx:MCParser.BlockstmtContext):
        return Block(self.visit(ctx.vardecl_stmt_list()))

    def visitVardecl_stmt_list(self,ctx:MCParser.Vardecl_stmt_listContext):
        result = [self.visit(x) for x in ctx.vardecl_stmt()]
        flat_result = flatten(result)
        return flat_result

    def visitVardecl_stmt(self,ctx:MCParser.Vardecl_stmtContext):
        if ctx.vardecl():
            return self.visit(ctx.vardecl())
        else:
            return self.visit(ctx.stmt())

    def visitSmtm(self,ctx:MCParser.StmtContext):
        return self.visit(ctx.getChild(0))

    def visitIfstmt(self,ctx:MCParser.IfstmtContext):
        return self.visit(ctx.getChild(0))

    def visitIfelsestmt(self,ctx:MCParser.IfelsestmtContext):
        return If(self.visit(ctx.exp()),self.visit(ctx.stmt(0)),self.visit(ctx.stmt(1)))

    def visitIfnoelsestmt(self,ctx:MCParser.IfnoelsestmtContext):
        return If(self.visit(ctx.exp()),self.visit(ctx.stmt()))

    def visitDowhilestmt(self,ctx:MCParser.DowhilestmtContext):
        return Dowhile(self.visit(ctx.stmtlist()),self.visit(ctx.exp()))

    def visitStmtlist(self,ctx:MCParser.StmtlistContext):
        return [self.visit(x) for x in ctx.stmt()]

    def visitForstmt(self,ctx:MCParser.ForstmtContext):
        return For(self.visit(ctx.exp(0)),self.visit(ctx.exp(1)),self.visit(ctx.exp(2)),self.visit(ctx.stmt()))

    def visitBreakstmt(self,ctx:MCParser.BreakstmtContext):
        return Break()

    def visitContinuestmt(self,ctx:MCParser.ContinuestmtContext):
        return Continue()

    def visitReturnstmt(self,ctx:MCParser.ReturnstmtContext):
        if ctx.exp():
            return Return(self.visit(ctx.exp()))
        else:
            return Return()

    def visitExpstmt(self,ctx:MCParser.ExpstmtContext):
        return self.visit(ctx.exp())

    def visitExp(self,ctx:MCParser.ExpContext):
        rl = ctx.exp1()[::-1]
        cl = zip(ctx.ASSIGN()[::-1],rl[1:])
        return reduce( lambda x,y: BinaryOp(y[0].getText(),self.visit(y[1]),x),cl,self.visit(rl[0]))

    def visitExp1(self,ctx:MCParser.Exp1Context):
        dl = zip(ctx.OR(),ctx.exp2()[1:])
        return reduce( lambda x,y: BinaryOp(y[0].getText(),x,self.visit(y[1])),dl,self.visit(ctx.exp2(0))) 

    def visitExp2(self,ctx:MCParser.Exp2Context):
        dl = zip(ctx.AND(),ctx.exp3()[1:])
        return reduce( lambda x,y: BinaryOp(y[0].getText(),x,self.visit(y[1])),dl,self.visit(ctx.exp3(0))) 

    def visitExp3(self,ctx:MCParser.Exp3Context):
        if ctx.getChildCount() != 1:
            return BinaryOp(ctx.getChild(1).getText(),self.visit(ctx.exp4(0)),self.visit(ctx.exp4(1)))
        else:
            return self.visit(ctx.exp4(0))

    def visitExp4(self,ctx:MCParser.Exp4Context):
        if ctx.getChildCount() != 1:
            return BinaryOp(ctx.getChild(1).getText(),self.visit(ctx.exp5(0)),self.visit(ctx.exp5(1)))
        else:
            return self.visit(ctx.exp5(0))

    def visitExp5(self,ctx:MCParser.Exp5Context):
        op = [x for x in ctx.children[1::2]]
        dl = zip(op,ctx.exp6()[1:])
        return reduce( lambda x,y: BinaryOp(y[0].getText(),x,self.visit(y[1])),dl,self.visit(ctx.exp6(0))) 

    def visitExp6(self,ctx:MCParser.Exp6Context):
        op = [x for x in ctx.children[1::2]]
        dl = zip(op,ctx.exp7()[1:])
        return reduce( lambda x,y: BinaryOp(y[0].getText(),x,self.visit(y[1])),dl,self.visit(ctx.exp7(0))) 

    def visitExp7(self,ctx:MCParser.Exp7Context):
        if ctx.exp7():
            return UnaryOp(ctx.getChild(0).getText(),self.visit(ctx.exp7()))
        else:
            return self.visit(ctx.arrele())

    def visitArrele(self,ctx:MCParser.ArreleContext):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp8())
        else:
            return ArrayCell( self.visit(ctx.exp8()),self.visit(ctx.exp()) )

    def visitExp8(self,ctx:MCParser.Exp8Context):
        if ctx.INTLIT():
            return IntLiteral(int(ctx.INTLIT().getText()))
        if ctx.FLOATLIT():
            return FloatLiteral( float(ctx.FLOATLIT().getText()) )
        if ctx.STRINGLIT():
            return StringLiteral(ctx.STRINGLIT().getText())
        if ctx.BOOLEANLIT():
            if ctx.BOOLEANLIT().getText() == 'true':
                return BooleanLiteral(True)
            else:
                return BooleanLiteral(False)
        if ctx.ID():
            return Id(ctx.ID().getText())
        else:
            return self.visit(ctx.getChild(0))

    def visitFuncall(self,ctx:MCParser.FuncallContext):
        return CallExpr( Id(ctx.ID().getText()),self.visit(ctx.explist()) )

    def visitExplist(self,ctx:MCParser.ExplistContext):
        return [self.visit(x) for x in ctx.exp()]

    def visitSubexp(self,ctx:MCParser.SubexpContext):
        return self.visit(ctx.exp())


def flatten(lst):
    if lst:
        if isinstance(lst[0],list):
            return lst[0] + flatten(lst[1:])
        else:
            return [lst[0]] + flatten(lst[1:])
    else:
        return []


