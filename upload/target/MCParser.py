# Generated from main/mc/parser/MC.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\63")
        buf.write("\u0144\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\3\2\3\2\3\2\3\3\6\3S\n\3\r\3\16\3T\3\4\3\4\5")
        buf.write("\4Y\n\4\3\5\3\5\3\5\3\5\7\5_\n\5\f\5\16\5b\13\5\3\5\3")
        buf.write("\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7\5\7m\n\7\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\t\3\t\3\t\5\ty\n\t\3\n\3\n\3\n\7\n~\n")
        buf.write("\n\f\n\16\n\u0081\13\n\5\n\u0083\n\n\3\13\3\13\3\13\3")
        buf.write("\13\3\13\3\13\3\13\3\13\5\13\u008d\n\13\3\f\3\f\3\f\3")
        buf.write("\f\3\r\3\r\5\r\u0095\n\r\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\21\6\21\u00ac\n\21\r\21\16\21\u00ad")
        buf.write("\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23")
        buf.write("\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25")
        buf.write("\5\25\u00c6\n\25\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3")
        buf.write("\30\7\30\u00d0\n\30\f\30\16\30\u00d3\13\30\3\31\3\31\5")
        buf.write("\31\u00d7\n\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write("\5\32\u00e1\n\32\3\33\3\33\3\33\7\33\u00e6\n\33\f\33\16")
        buf.write("\33\u00e9\13\33\3\33\3\33\3\34\3\34\3\34\7\34\u00f0\n")
        buf.write("\34\f\34\16\34\u00f3\13\34\3\35\3\35\3\35\7\35\u00f8\n")
        buf.write("\35\f\35\16\35\u00fb\13\35\3\36\3\36\3\36\3\36\3\36\5")
        buf.write("\36\u0102\n\36\3\37\3\37\3\37\3\37\3\37\5\37\u0109\n\37")
        buf.write("\3 \3 \3 \7 \u010e\n \f \16 \u0111\13 \3!\3!\3!\7!\u0116")
        buf.write("\n!\f!\16!\u0119\13!\3\"\3\"\3\"\5\"\u011e\n\"\3#\3#\3")
        buf.write("#\3#\3#\3#\5#\u0126\n#\3$\3$\3$\3$\3$\3$\3$\5$\u012f\n")
        buf.write("$\3%\3%\3%\3%\3%\3&\3&\3&\7&\u0139\n&\f&\16&\u013c\13")
        buf.write("&\5&\u013e\n&\3\'\3\'\3\'\3\'\3\'\2\2(\2\4\6\b\n\f\16")
        buf.write("\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDF")
        buf.write("HJL\2\b\4\2\7\7\t\13\3\2\'(\3\2),\3\2\37 \3\2!#\4\2  ")
        buf.write("$$\2\u0143\2N\3\2\2\2\4R\3\2\2\2\6X\3\2\2\2\bZ\3\2\2\2")
        buf.write("\ne\3\2\2\2\fl\3\2\2\2\16n\3\2\2\2\20x\3\2\2\2\22\u0082")
        buf.write("\3\2\2\2\24\u008c\3\2\2\2\26\u008e\3\2\2\2\30\u0094\3")
        buf.write("\2\2\2\32\u0096\3\2\2\2\34\u009c\3\2\2\2\36\u00a4\3\2")
        buf.write("\2\2 \u00ab\3\2\2\2\"\u00af\3\2\2\2$\u00b9\3\2\2\2&\u00bc")
        buf.write("\3\2\2\2(\u00c5\3\2\2\2*\u00c7\3\2\2\2,\u00ca\3\2\2\2")
        buf.write(".\u00d1\3\2\2\2\60\u00d6\3\2\2\2\62\u00e0\3\2\2\2\64\u00e7")
        buf.write("\3\2\2\2\66\u00ec\3\2\2\28\u00f4\3\2\2\2:\u0101\3\2\2")
        buf.write("\2<\u0108\3\2\2\2>\u010a\3\2\2\2@\u0112\3\2\2\2B\u011d")
        buf.write("\3\2\2\2D\u0125\3\2\2\2F\u012e\3\2\2\2H\u0130\3\2\2\2")
        buf.write("J\u013d\3\2\2\2L\u013f\3\2\2\2NO\5\4\3\2OP\7\2\2\3P\3")
        buf.write("\3\2\2\2QS\5\6\4\2RQ\3\2\2\2ST\3\2\2\2TR\3\2\2\2TU\3\2")
        buf.write("\2\2U\5\3\2\2\2VY\5\b\5\2WY\5\16\b\2XV\3\2\2\2XW\3\2\2")
        buf.write("\2Y\7\3\2\2\2Z[\5\n\6\2[`\5\f\7\2\\]\7\36\2\2]_\5\f\7")
        buf.write("\2^\\\3\2\2\2_b\3\2\2\2`^\3\2\2\2`a\3\2\2\2ac\3\2\2\2")
        buf.write("b`\3\2\2\2cd\7\35\2\2d\t\3\2\2\2ef\t\2\2\2f\13\3\2\2\2")
        buf.write("gm\7\26\2\2hi\7\26\2\2ij\7\33\2\2jk\7\3\2\2km\7\34\2\2")
        buf.write("lg\3\2\2\2lh\3\2\2\2m\r\3\2\2\2no\5\20\t\2op\7\26\2\2")
        buf.write("pq\7\27\2\2qr\5\22\n\2rs\7\30\2\2st\5,\27\2t\17\3\2\2")
        buf.write("\2uy\5\n\6\2vy\5\26\f\2wy\7\b\2\2xu\3\2\2\2xv\3\2\2\2")
        buf.write("xw\3\2\2\2y\21\3\2\2\2z\177\5\24\13\2{|\7\36\2\2|~\5\24")
        buf.write("\13\2}{\3\2\2\2~\u0081\3\2\2\2\177}\3\2\2\2\177\u0080")
        buf.write("\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2\2\u0082z")
        buf.write("\3\2\2\2\u0082\u0083\3\2\2\2\u0083\23\3\2\2\2\u0084\u0085")
        buf.write("\5\n\6\2\u0085\u0086\7\26\2\2\u0086\u008d\3\2\2\2\u0087")
        buf.write("\u0088\5\n\6\2\u0088\u0089\7\26\2\2\u0089\u008a\7\33\2")
        buf.write("\2\u008a\u008b\7\34\2\2\u008b\u008d\3\2\2\2\u008c\u0084")
        buf.write("\3\2\2\2\u008c\u0087\3\2\2\2\u008d\25\3\2\2\2\u008e\u008f")
        buf.write("\5\n\6\2\u008f\u0090\7\33\2\2\u0090\u0091\7\34\2\2\u0091")
        buf.write("\27\3\2\2\2\u0092\u0095\5\32\16\2\u0093\u0095\5\34\17")
        buf.write("\2\u0094\u0092\3\2\2\2\u0094\u0093\3\2\2\2\u0095\31\3")
        buf.write("\2\2\2\u0096\u0097\7\20\2\2\u0097\u0098\7\27\2\2\u0098")
        buf.write("\u0099\5\64\33\2\u0099\u009a\7\30\2\2\u009a\u009b\5\62")
        buf.write("\32\2\u009b\33\3\2\2\2\u009c\u009d\7\20\2\2\u009d\u009e")
        buf.write("\7\27\2\2\u009e\u009f\5\64\33\2\u009f\u00a0\7\30\2\2\u00a0")
        buf.write("\u00a1\5\62\32\2\u00a1\u00a2\7\16\2\2\u00a2\u00a3\5\62")
        buf.write("\32\2\u00a3\35\3\2\2\2\u00a4\u00a5\7\22\2\2\u00a5\u00a6")
        buf.write("\5 \21\2\u00a6\u00a7\7\23\2\2\u00a7\u00a8\5\64\33\2\u00a8")
        buf.write("\u00a9\7\35\2\2\u00a9\37\3\2\2\2\u00aa\u00ac\5\62\32\2")
        buf.write("\u00ab\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\u00ab\3")
        buf.write("\2\2\2\u00ad\u00ae\3\2\2\2\u00ae!\3\2\2\2\u00af\u00b0")
        buf.write("\7\17\2\2\u00b0\u00b1\7\27\2\2\u00b1\u00b2\5\64\33\2\u00b2")
        buf.write("\u00b3\7\35\2\2\u00b3\u00b4\5\64\33\2\u00b4\u00b5\7\35")
        buf.write("\2\2\u00b5\u00b6\5\64\33\2\u00b6\u00b7\7\30\2\2\u00b7")
        buf.write("\u00b8\5\62\32\2\u00b8#\3\2\2\2\u00b9\u00ba\7\f\2\2\u00ba")
        buf.write("\u00bb\7\35\2\2\u00bb%\3\2\2\2\u00bc\u00bd\7\r\2\2\u00bd")
        buf.write("\u00be\7\35\2\2\u00be\'\3\2\2\2\u00bf\u00c0\7\21\2\2\u00c0")
        buf.write("\u00c6\7\35\2\2\u00c1\u00c2\7\21\2\2\u00c2\u00c3\5\64")
        buf.write("\33\2\u00c3\u00c4\7\35\2\2\u00c4\u00c6\3\2\2\2\u00c5\u00bf")
        buf.write("\3\2\2\2\u00c5\u00c1\3\2\2\2\u00c6)\3\2\2\2\u00c7\u00c8")
        buf.write("\5\64\33\2\u00c8\u00c9\7\35\2\2\u00c9+\3\2\2\2\u00ca\u00cb")
        buf.write("\7\31\2\2\u00cb\u00cc\5.\30\2\u00cc\u00cd\7\32\2\2\u00cd")
        buf.write("-\3\2\2\2\u00ce\u00d0\5\60\31\2\u00cf\u00ce\3\2\2\2\u00d0")
        buf.write("\u00d3\3\2\2\2\u00d1\u00cf\3\2\2\2\u00d1\u00d2\3\2\2\2")
        buf.write("\u00d2/\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d4\u00d7\5\b\5")
        buf.write("\2\u00d5\u00d7\5\62\32\2\u00d6\u00d4\3\2\2\2\u00d6\u00d5")
        buf.write("\3\2\2\2\u00d7\61\3\2\2\2\u00d8\u00e1\5\30\r\2\u00d9\u00e1")
        buf.write("\5\36\20\2\u00da\u00e1\5\"\22\2\u00db\u00e1\5$\23\2\u00dc")
        buf.write("\u00e1\5&\24\2\u00dd\u00e1\5(\25\2\u00de\u00e1\5*\26\2")
        buf.write("\u00df\u00e1\5,\27\2\u00e0\u00d8\3\2\2\2\u00e0\u00d9\3")
        buf.write("\2\2\2\u00e0\u00da\3\2\2\2\u00e0\u00db\3\2\2\2\u00e0\u00dc")
        buf.write("\3\2\2\2\u00e0\u00dd\3\2\2\2\u00e0\u00de\3\2\2\2\u00e0")
        buf.write("\u00df\3\2\2\2\u00e1\63\3\2\2\2\u00e2\u00e3\5\66\34\2")
        buf.write("\u00e3\u00e4\7-\2\2\u00e4\u00e6\3\2\2\2\u00e5\u00e2\3")
        buf.write("\2\2\2\u00e6\u00e9\3\2\2\2\u00e7\u00e5\3\2\2\2\u00e7\u00e8")
        buf.write("\3\2\2\2\u00e8\u00ea\3\2\2\2\u00e9\u00e7\3\2\2\2\u00ea")
        buf.write("\u00eb\5\66\34\2\u00eb\65\3\2\2\2\u00ec\u00f1\58\35\2")
        buf.write("\u00ed\u00ee\7%\2\2\u00ee\u00f0\58\35\2\u00ef\u00ed\3")
        buf.write("\2\2\2\u00f0\u00f3\3\2\2\2\u00f1\u00ef\3\2\2\2\u00f1\u00f2")
        buf.write("\3\2\2\2\u00f2\67\3\2\2\2\u00f3\u00f1\3\2\2\2\u00f4\u00f9")
        buf.write("\5:\36\2\u00f5\u00f6\7&\2\2\u00f6\u00f8\5:\36\2\u00f7")
        buf.write("\u00f5\3\2\2\2\u00f8\u00fb\3\2\2\2\u00f9\u00f7\3\2\2\2")
        buf.write("\u00f9\u00fa\3\2\2\2\u00fa9\3\2\2\2\u00fb\u00f9\3\2\2")
        buf.write("\2\u00fc\u00fd\5<\37\2\u00fd\u00fe\t\3\2\2\u00fe\u00ff")
        buf.write("\5<\37\2\u00ff\u0102\3\2\2\2\u0100\u0102\5<\37\2\u0101")
        buf.write("\u00fc\3\2\2\2\u0101\u0100\3\2\2\2\u0102;\3\2\2\2\u0103")
        buf.write("\u0104\5> \2\u0104\u0105\t\4\2\2\u0105\u0106\5> \2\u0106")
        buf.write("\u0109\3\2\2\2\u0107\u0109\5> \2\u0108\u0103\3\2\2\2\u0108")
        buf.write("\u0107\3\2\2\2\u0109=\3\2\2\2\u010a\u010f\5@!\2\u010b")
        buf.write("\u010c\t\5\2\2\u010c\u010e\5@!\2\u010d\u010b\3\2\2\2\u010e")
        buf.write("\u0111\3\2\2\2\u010f\u010d\3\2\2\2\u010f\u0110\3\2\2\2")
        buf.write("\u0110?\3\2\2\2\u0111\u010f\3\2\2\2\u0112\u0117\5B\"\2")
        buf.write("\u0113\u0114\t\6\2\2\u0114\u0116\5B\"\2\u0115\u0113\3")
        buf.write("\2\2\2\u0116\u0119\3\2\2\2\u0117\u0115\3\2\2\2\u0117\u0118")
        buf.write("\3\2\2\2\u0118A\3\2\2\2\u0119\u0117\3\2\2\2\u011a\u011b")
        buf.write("\t\7\2\2\u011b\u011e\5B\"\2\u011c\u011e\5D#\2\u011d\u011a")
        buf.write("\3\2\2\2\u011d\u011c\3\2\2\2\u011eC\3\2\2\2\u011f\u0120")
        buf.write("\5F$\2\u0120\u0121\7\33\2\2\u0121\u0122\5\64\33\2\u0122")
        buf.write("\u0123\7\34\2\2\u0123\u0126\3\2\2\2\u0124\u0126\5F$\2")
        buf.write("\u0125\u011f\3\2\2\2\u0125\u0124\3\2\2\2\u0126E\3\2\2")
        buf.write("\2\u0127\u012f\7\3\2\2\u0128\u012f\7\5\2\2\u0129\u012f")
        buf.write("\7\4\2\2\u012a\u012f\7\6\2\2\u012b\u012f\7\26\2\2\u012c")
        buf.write("\u012f\5L\'\2\u012d\u012f\5H%\2\u012e\u0127\3\2\2\2\u012e")
        buf.write("\u0128\3\2\2\2\u012e\u0129\3\2\2\2\u012e\u012a\3\2\2\2")
        buf.write("\u012e\u012b\3\2\2\2\u012e\u012c\3\2\2\2\u012e\u012d\3")
        buf.write("\2\2\2\u012fG\3\2\2\2\u0130\u0131\7\26\2\2\u0131\u0132")
        buf.write("\7\27\2\2\u0132\u0133\5J&\2\u0133\u0134\7\30\2\2\u0134")
        buf.write("I\3\2\2\2\u0135\u013a\5\64\33\2\u0136\u0137\7\36\2\2\u0137")
        buf.write("\u0139\5\64\33\2\u0138\u0136\3\2\2\2\u0139\u013c\3\2\2")
        buf.write("\2\u013a\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u013e")
        buf.write("\3\2\2\2\u013c\u013a\3\2\2\2\u013d\u0135\3\2\2\2\u013d")
        buf.write("\u013e\3\2\2\2\u013eK\3\2\2\2\u013f\u0140\7\27\2\2\u0140")
        buf.write("\u0141\5\64\33\2\u0141\u0142\7\30\2\2\u0142M\3\2\2\2\34")
        buf.write("TX`lx\177\u0082\u008c\u0094\u00ad\u00c5\u00d1\u00d6\u00e0")
        buf.write("\u00e7\u00f1\u00f9\u0101\u0108\u010f\u0117\u011d\u0125")
        buf.write("\u012e\u013a\u013d")
        return buf.getvalue()


class MCParser ( Parser ):

    grammarFileName = "MC.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "'int'", "'void'", "'float'", "'boolean'", 
                     "'string'", "'break'", "'continue'", "'else'", "'for'", 
                     "'if'", "'return'", "'do'", "'while'", "'true'", "'false'", 
                     "<INVALID>", "'('", "')'", "'{'", "'}'", "'['", "']'", 
                     "';'", "','", "'+'", "'-'", "'*'", "'/'", "'%'", "'!'", 
                     "'||'", "'&&'", "'=='", "'!='", "'<='", "'>='", "'<'", 
                     "'>'", "'='" ]

    symbolicNames = [ "<INVALID>", "INTLIT", "FLOATLIT", "STRINGLIT", "BOOLEANLIT", 
                      "INTTYPE", "VOIDTYPE", "FLOATTYPE", "BOOLTYPE", "STRINGTYPE", 
                      "BREAK", "CONT", "ELSE", "FOR", "IF", "RETURN", "DO", 
                      "WHILE", "TRUE", "FALSE", "ID", "LB", "RB", "LP", 
                      "RP", "LSB", "RSB", "SM", "CM", "ADD", "SUB", "MUL", 
                      "DIV", "MOD", "NOT", "OR", "AND", "EQ", "NEQ", "LTE", 
                      "GTE", "LT", "GT", "ASSIGN", "LINE_CMT", "BLOCK_CMT", 
                      "WS", "ERROR_CHAR", "UNCLOSE_STRING", "ILLEGAL_ESCAPE" ]

    RULE_program = 0
    RULE_manydecls = 1
    RULE_decl = 2
    RULE_vardecl = 3
    RULE_primtype = 4
    RULE_var = 5
    RULE_funcdecl = 6
    RULE_types = 7
    RULE_paramlist = 8
    RULE_paramdecl = 9
    RULE_arrptype = 10
    RULE_ifstmt = 11
    RULE_ifnoelsestmt = 12
    RULE_ifelsestmt = 13
    RULE_dowhilestmt = 14
    RULE_stmtlist = 15
    RULE_forstmt = 16
    RULE_breakstmt = 17
    RULE_continuestmt = 18
    RULE_returnstmt = 19
    RULE_expstmt = 20
    RULE_blockstmt = 21
    RULE_vardecl_stmt_list = 22
    RULE_vardecl_stmt = 23
    RULE_stmt = 24
    RULE_exp = 25
    RULE_exp1 = 26
    RULE_exp2 = 27
    RULE_exp3 = 28
    RULE_exp4 = 29
    RULE_exp5 = 30
    RULE_exp6 = 31
    RULE_exp7 = 32
    RULE_arrele = 33
    RULE_exp8 = 34
    RULE_funcall = 35
    RULE_explist = 36
    RULE_subexp = 37

    ruleNames =  [ "program", "manydecls", "decl", "vardecl", "primtype", 
                   "var", "funcdecl", "types", "paramlist", "paramdecl", 
                   "arrptype", "ifstmt", "ifnoelsestmt", "ifelsestmt", "dowhilestmt", 
                   "stmtlist", "forstmt", "breakstmt", "continuestmt", "returnstmt", 
                   "expstmt", "blockstmt", "vardecl_stmt_list", "vardecl_stmt", 
                   "stmt", "exp", "exp1", "exp2", "exp3", "exp4", "exp5", 
                   "exp6", "exp7", "arrele", "exp8", "funcall", "explist", 
                   "subexp" ]

    EOF = Token.EOF
    INTLIT=1
    FLOATLIT=2
    STRINGLIT=3
    BOOLEANLIT=4
    INTTYPE=5
    VOIDTYPE=6
    FLOATTYPE=7
    BOOLTYPE=8
    STRINGTYPE=9
    BREAK=10
    CONT=11
    ELSE=12
    FOR=13
    IF=14
    RETURN=15
    DO=16
    WHILE=17
    TRUE=18
    FALSE=19
    ID=20
    LB=21
    RB=22
    LP=23
    RP=24
    LSB=25
    RSB=26
    SM=27
    CM=28
    ADD=29
    SUB=30
    MUL=31
    DIV=32
    MOD=33
    NOT=34
    OR=35
    AND=36
    EQ=37
    NEQ=38
    LTE=39
    GTE=40
    LT=41
    GT=42
    ASSIGN=43
    LINE_CMT=44
    BLOCK_CMT=45
    WS=46
    ERROR_CHAR=47
    UNCLOSE_STRING=48
    ILLEGAL_ESCAPE=49

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def manydecls(self):
            return self.getTypedRuleContext(MCParser.ManydeclsContext,0)


        def EOF(self):
            return self.getToken(MCParser.EOF, 0)

        def getRuleIndex(self):
            return MCParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = MCParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 76
            self.manydecls()
            self.state = 77
            self.match(MCParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ManydeclsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.DeclContext)
            else:
                return self.getTypedRuleContext(MCParser.DeclContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_manydecls

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitManydecls" ):
                return visitor.visitManydecls(self)
            else:
                return visitor.visitChildren(self)




    def manydecls(self):

        localctx = MCParser.ManydeclsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_manydecls)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 80 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 79
                self.decl()
                self.state = 82 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTTYPE) | (1 << MCParser.VOIDTYPE) | (1 << MCParser.FLOATTYPE) | (1 << MCParser.BOOLTYPE) | (1 << MCParser.STRINGTYPE))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def vardecl(self):
            return self.getTypedRuleContext(MCParser.VardeclContext,0)


        def funcdecl(self):
            return self.getTypedRuleContext(MCParser.FuncdeclContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDecl" ):
                return visitor.visitDecl(self)
            else:
                return visitor.visitChildren(self)




    def decl(self):

        localctx = MCParser.DeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_decl)
        try:
            self.state = 86
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 84
                self.vardecl()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 85
                self.funcdecl()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VardeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primtype(self):
            return self.getTypedRuleContext(MCParser.PrimtypeContext,0)


        def var(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.VarContext)
            else:
                return self.getTypedRuleContext(MCParser.VarContext,i)


        def SM(self):
            return self.getToken(MCParser.SM, 0)

        def CM(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.CM)
            else:
                return self.getToken(MCParser.CM, i)

        def getRuleIndex(self):
            return MCParser.RULE_vardecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVardecl" ):
                return visitor.visitVardecl(self)
            else:
                return visitor.visitChildren(self)




    def vardecl(self):

        localctx = MCParser.VardeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_vardecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 88
            self.primtype()
            self.state = 89
            self.var()
            self.state = 94
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MCParser.CM:
                self.state = 90
                self.match(MCParser.CM)
                self.state = 91
                self.var()
                self.state = 96
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 97
            self.match(MCParser.SM)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrimtypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BOOLTYPE(self):
            return self.getToken(MCParser.BOOLTYPE, 0)

        def INTTYPE(self):
            return self.getToken(MCParser.INTTYPE, 0)

        def FLOATTYPE(self):
            return self.getToken(MCParser.FLOATTYPE, 0)

        def STRINGTYPE(self):
            return self.getToken(MCParser.STRINGTYPE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_primtype

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimtype" ):
                return visitor.visitPrimtype(self)
            else:
                return visitor.visitChildren(self)




    def primtype(self):

        localctx = MCParser.PrimtypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_primtype)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 99
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTTYPE) | (1 << MCParser.FLOATTYPE) | (1 << MCParser.BOOLTYPE) | (1 << MCParser.STRINGTYPE))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def INTLIT(self):
            return self.getToken(MCParser.INTLIT, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_var

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar" ):
                return visitor.visitVar(self)
            else:
                return visitor.visitChildren(self)




    def var(self):

        localctx = MCParser.VarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_var)
        try:
            self.state = 106
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 101
                self.match(MCParser.ID)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 102
                self.match(MCParser.ID)
                self.state = 103
                self.match(MCParser.LSB)
                self.state = 104
                self.match(MCParser.INTLIT)
                self.state = 105
                self.match(MCParser.RSB)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FuncdeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def types(self):
            return self.getTypedRuleContext(MCParser.TypesContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def paramlist(self):
            return self.getTypedRuleContext(MCParser.ParamlistContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def blockstmt(self):
            return self.getTypedRuleContext(MCParser.BlockstmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_funcdecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncdecl" ):
                return visitor.visitFuncdecl(self)
            else:
                return visitor.visitChildren(self)




    def funcdecl(self):

        localctx = MCParser.FuncdeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_funcdecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 108
            self.types()
            self.state = 109
            self.match(MCParser.ID)
            self.state = 110
            self.match(MCParser.LB)
            self.state = 111
            self.paramlist()
            self.state = 112
            self.match(MCParser.RB)
            self.state = 113
            self.blockstmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TypesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primtype(self):
            return self.getTypedRuleContext(MCParser.PrimtypeContext,0)


        def arrptype(self):
            return self.getTypedRuleContext(MCParser.ArrptypeContext,0)


        def VOIDTYPE(self):
            return self.getToken(MCParser.VOIDTYPE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_types

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTypes" ):
                return visitor.visitTypes(self)
            else:
                return visitor.visitChildren(self)




    def types(self):

        localctx = MCParser.TypesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_types)
        try:
            self.state = 118
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 115
                self.primtype()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 116
                self.arrptype()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 117
                self.match(MCParser.VOIDTYPE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamlistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def paramdecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ParamdeclContext)
            else:
                return self.getTypedRuleContext(MCParser.ParamdeclContext,i)


        def CM(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.CM)
            else:
                return self.getToken(MCParser.CM, i)

        def getRuleIndex(self):
            return MCParser.RULE_paramlist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParamlist" ):
                return visitor.visitParamlist(self)
            else:
                return visitor.visitChildren(self)




    def paramlist(self):

        localctx = MCParser.ParamlistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_paramlist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 128
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTTYPE) | (1 << MCParser.FLOATTYPE) | (1 << MCParser.BOOLTYPE) | (1 << MCParser.STRINGTYPE))) != 0):
                self.state = 120
                self.paramdecl()
                self.state = 125
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MCParser.CM:
                    self.state = 121
                    self.match(MCParser.CM)
                    self.state = 122
                    self.paramdecl()
                    self.state = 127
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamdeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primtype(self):
            return self.getTypedRuleContext(MCParser.PrimtypeContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_paramdecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParamdecl" ):
                return visitor.visitParamdecl(self)
            else:
                return visitor.visitChildren(self)




    def paramdecl(self):

        localctx = MCParser.ParamdeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_paramdecl)
        try:
            self.state = 138
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 130
                self.primtype()
                self.state = 131
                self.match(MCParser.ID)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 133
                self.primtype()
                self.state = 134
                self.match(MCParser.ID)
                self.state = 135
                self.match(MCParser.LSB)
                self.state = 136
                self.match(MCParser.RSB)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArrptypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primtype(self):
            return self.getTypedRuleContext(MCParser.PrimtypeContext,0)


        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_arrptype

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArrptype" ):
                return visitor.visitArrptype(self)
            else:
                return visitor.visitChildren(self)




    def arrptype(self):

        localctx = MCParser.ArrptypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_arrptype)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 140
            self.primtype()
            self.state = 141
            self.match(MCParser.LSB)
            self.state = 142
            self.match(MCParser.RSB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfstmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ifnoelsestmt(self):
            return self.getTypedRuleContext(MCParser.IfnoelsestmtContext,0)


        def ifelsestmt(self):
            return self.getTypedRuleContext(MCParser.IfelsestmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_ifstmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfstmt" ):
                return visitor.visitIfstmt(self)
            else:
                return visitor.visitChildren(self)




    def ifstmt(self):

        localctx = MCParser.IfstmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_ifstmt)
        try:
            self.state = 146
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 144
                self.ifnoelsestmt()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 145
                self.ifelsestmt()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfnoelsestmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(MCParser.IF, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def stmt(self):
            return self.getTypedRuleContext(MCParser.StmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_ifnoelsestmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfnoelsestmt" ):
                return visitor.visitIfnoelsestmt(self)
            else:
                return visitor.visitChildren(self)




    def ifnoelsestmt(self):

        localctx = MCParser.IfnoelsestmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_ifnoelsestmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 148
            self.match(MCParser.IF)
            self.state = 149
            self.match(MCParser.LB)
            self.state = 150
            self.exp()
            self.state = 151
            self.match(MCParser.RB)
            self.state = 152
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfelsestmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(MCParser.IF, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StmtContext)
            else:
                return self.getTypedRuleContext(MCParser.StmtContext,i)


        def ELSE(self):
            return self.getToken(MCParser.ELSE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_ifelsestmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfelsestmt" ):
                return visitor.visitIfelsestmt(self)
            else:
                return visitor.visitChildren(self)




    def ifelsestmt(self):

        localctx = MCParser.IfelsestmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_ifelsestmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 154
            self.match(MCParser.IF)
            self.state = 155
            self.match(MCParser.LB)
            self.state = 156
            self.exp()
            self.state = 157
            self.match(MCParser.RB)
            self.state = 158
            self.stmt()
            self.state = 159
            self.match(MCParser.ELSE)
            self.state = 160
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DowhilestmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DO(self):
            return self.getToken(MCParser.DO, 0)

        def stmtlist(self):
            return self.getTypedRuleContext(MCParser.StmtlistContext,0)


        def WHILE(self):
            return self.getToken(MCParser.WHILE, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def SM(self):
            return self.getToken(MCParser.SM, 0)

        def getRuleIndex(self):
            return MCParser.RULE_dowhilestmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDowhilestmt" ):
                return visitor.visitDowhilestmt(self)
            else:
                return visitor.visitChildren(self)




    def dowhilestmt(self):

        localctx = MCParser.DowhilestmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_dowhilestmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 162
            self.match(MCParser.DO)
            self.state = 163
            self.stmtlist()
            self.state = 164
            self.match(MCParser.WHILE)
            self.state = 165
            self.exp()
            self.state = 166
            self.match(MCParser.SM)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StmtlistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StmtContext)
            else:
                return self.getTypedRuleContext(MCParser.StmtContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_stmtlist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStmtlist" ):
                return visitor.visitStmtlist(self)
            else:
                return visitor.visitChildren(self)




    def stmtlist(self):

        localctx = MCParser.StmtlistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_stmtlist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 169 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 168
                self.stmt()
                self.state = 171 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FLOATLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.BOOLEANLIT) | (1 << MCParser.BREAK) | (1 << MCParser.CONT) | (1 << MCParser.FOR) | (1 << MCParser.IF) | (1 << MCParser.RETURN) | (1 << MCParser.DO) | (1 << MCParser.ID) | (1 << MCParser.LB) | (1 << MCParser.LP) | (1 << MCParser.SUB) | (1 << MCParser.NOT))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ForstmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(MCParser.FOR, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExpContext)
            else:
                return self.getTypedRuleContext(MCParser.ExpContext,i)


        def SM(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.SM)
            else:
                return self.getToken(MCParser.SM, i)

        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def stmt(self):
            return self.getTypedRuleContext(MCParser.StmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_forstmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitForstmt" ):
                return visitor.visitForstmt(self)
            else:
                return visitor.visitChildren(self)




    def forstmt(self):

        localctx = MCParser.ForstmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_forstmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 173
            self.match(MCParser.FOR)
            self.state = 174
            self.match(MCParser.LB)
            self.state = 175
            self.exp()
            self.state = 176
            self.match(MCParser.SM)
            self.state = 177
            self.exp()
            self.state = 178
            self.match(MCParser.SM)
            self.state = 179
            self.exp()
            self.state = 180
            self.match(MCParser.RB)
            self.state = 181
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BreakstmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BREAK(self):
            return self.getToken(MCParser.BREAK, 0)

        def SM(self):
            return self.getToken(MCParser.SM, 0)

        def getRuleIndex(self):
            return MCParser.RULE_breakstmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBreakstmt" ):
                return visitor.visitBreakstmt(self)
            else:
                return visitor.visitChildren(self)




    def breakstmt(self):

        localctx = MCParser.BreakstmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_breakstmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 183
            self.match(MCParser.BREAK)
            self.state = 184
            self.match(MCParser.SM)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ContinuestmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONT(self):
            return self.getToken(MCParser.CONT, 0)

        def SM(self):
            return self.getToken(MCParser.SM, 0)

        def getRuleIndex(self):
            return MCParser.RULE_continuestmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitContinuestmt" ):
                return visitor.visitContinuestmt(self)
            else:
                return visitor.visitChildren(self)




    def continuestmt(self):

        localctx = MCParser.ContinuestmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_continuestmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 186
            self.match(MCParser.CONT)
            self.state = 187
            self.match(MCParser.SM)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReturnstmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(MCParser.RETURN, 0)

        def SM(self):
            return self.getToken(MCParser.SM, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_returnstmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReturnstmt" ):
                return visitor.visitReturnstmt(self)
            else:
                return visitor.visitChildren(self)




    def returnstmt(self):

        localctx = MCParser.ReturnstmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_returnstmt)
        try:
            self.state = 195
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 189
                self.match(MCParser.RETURN)
                self.state = 190
                self.match(MCParser.SM)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 191
                self.match(MCParser.RETURN)
                self.state = 192
                self.exp()
                self.state = 193
                self.match(MCParser.SM)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpstmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def SM(self):
            return self.getToken(MCParser.SM, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expstmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpstmt" ):
                return visitor.visitExpstmt(self)
            else:
                return visitor.visitChildren(self)




    def expstmt(self):

        localctx = MCParser.ExpstmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_expstmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 197
            self.exp()
            self.state = 198
            self.match(MCParser.SM)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BlockstmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LP(self):
            return self.getToken(MCParser.LP, 0)

        def vardecl_stmt_list(self):
            return self.getTypedRuleContext(MCParser.Vardecl_stmt_listContext,0)


        def RP(self):
            return self.getToken(MCParser.RP, 0)

        def getRuleIndex(self):
            return MCParser.RULE_blockstmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlockstmt" ):
                return visitor.visitBlockstmt(self)
            else:
                return visitor.visitChildren(self)




    def blockstmt(self):

        localctx = MCParser.BlockstmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_blockstmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 200
            self.match(MCParser.LP)
            self.state = 201
            self.vardecl_stmt_list()
            self.state = 202
            self.match(MCParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Vardecl_stmt_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def vardecl_stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Vardecl_stmtContext)
            else:
                return self.getTypedRuleContext(MCParser.Vardecl_stmtContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_vardecl_stmt_list

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVardecl_stmt_list" ):
                return visitor.visitVardecl_stmt_list(self)
            else:
                return visitor.visitChildren(self)




    def vardecl_stmt_list(self):

        localctx = MCParser.Vardecl_stmt_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_vardecl_stmt_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 207
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FLOATLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.BOOLEANLIT) | (1 << MCParser.INTTYPE) | (1 << MCParser.FLOATTYPE) | (1 << MCParser.BOOLTYPE) | (1 << MCParser.STRINGTYPE) | (1 << MCParser.BREAK) | (1 << MCParser.CONT) | (1 << MCParser.FOR) | (1 << MCParser.IF) | (1 << MCParser.RETURN) | (1 << MCParser.DO) | (1 << MCParser.ID) | (1 << MCParser.LB) | (1 << MCParser.LP) | (1 << MCParser.SUB) | (1 << MCParser.NOT))) != 0):
                self.state = 204
                self.vardecl_stmt()
                self.state = 209
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Vardecl_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def vardecl(self):
            return self.getTypedRuleContext(MCParser.VardeclContext,0)


        def stmt(self):
            return self.getTypedRuleContext(MCParser.StmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_vardecl_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVardecl_stmt" ):
                return visitor.visitVardecl_stmt(self)
            else:
                return visitor.visitChildren(self)




    def vardecl_stmt(self):

        localctx = MCParser.Vardecl_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_vardecl_stmt)
        try:
            self.state = 212
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.INTTYPE, MCParser.FLOATTYPE, MCParser.BOOLTYPE, MCParser.STRINGTYPE]:
                self.enterOuterAlt(localctx, 1)
                self.state = 210
                self.vardecl()
                pass
            elif token in [MCParser.INTLIT, MCParser.FLOATLIT, MCParser.STRINGLIT, MCParser.BOOLEANLIT, MCParser.BREAK, MCParser.CONT, MCParser.FOR, MCParser.IF, MCParser.RETURN, MCParser.DO, MCParser.ID, MCParser.LB, MCParser.LP, MCParser.SUB, MCParser.NOT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 211
                self.stmt()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ifstmt(self):
            return self.getTypedRuleContext(MCParser.IfstmtContext,0)


        def dowhilestmt(self):
            return self.getTypedRuleContext(MCParser.DowhilestmtContext,0)


        def forstmt(self):
            return self.getTypedRuleContext(MCParser.ForstmtContext,0)


        def breakstmt(self):
            return self.getTypedRuleContext(MCParser.BreakstmtContext,0)


        def continuestmt(self):
            return self.getTypedRuleContext(MCParser.ContinuestmtContext,0)


        def returnstmt(self):
            return self.getTypedRuleContext(MCParser.ReturnstmtContext,0)


        def expstmt(self):
            return self.getTypedRuleContext(MCParser.ExpstmtContext,0)


        def blockstmt(self):
            return self.getTypedRuleContext(MCParser.BlockstmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStmt" ):
                return visitor.visitStmt(self)
            else:
                return visitor.visitChildren(self)




    def stmt(self):

        localctx = MCParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_stmt)
        try:
            self.state = 222
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.IF]:
                self.enterOuterAlt(localctx, 1)
                self.state = 214
                self.ifstmt()
                pass
            elif token in [MCParser.DO]:
                self.enterOuterAlt(localctx, 2)
                self.state = 215
                self.dowhilestmt()
                pass
            elif token in [MCParser.FOR]:
                self.enterOuterAlt(localctx, 3)
                self.state = 216
                self.forstmt()
                pass
            elif token in [MCParser.BREAK]:
                self.enterOuterAlt(localctx, 4)
                self.state = 217
                self.breakstmt()
                pass
            elif token in [MCParser.CONT]:
                self.enterOuterAlt(localctx, 5)
                self.state = 218
                self.continuestmt()
                pass
            elif token in [MCParser.RETURN]:
                self.enterOuterAlt(localctx, 6)
                self.state = 219
                self.returnstmt()
                pass
            elif token in [MCParser.INTLIT, MCParser.FLOATLIT, MCParser.STRINGLIT, MCParser.BOOLEANLIT, MCParser.ID, MCParser.LB, MCParser.SUB, MCParser.NOT]:
                self.enterOuterAlt(localctx, 7)
                self.state = 220
                self.expstmt()
                pass
            elif token in [MCParser.LP]:
                self.enterOuterAlt(localctx, 8)
                self.state = 221
                self.blockstmt()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp1(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp1Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp1Context,i)


        def ASSIGN(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.ASSIGN)
            else:
                return self.getToken(MCParser.ASSIGN, i)

        def getRuleIndex(self):
            return MCParser.RULE_exp

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp" ):
                return visitor.visitExp(self)
            else:
                return visitor.visitChildren(self)




    def exp(self):

        localctx = MCParser.ExpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_exp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 229
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,14,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 224
                    self.exp1()
                    self.state = 225
                    self.match(MCParser.ASSIGN) 
                self.state = 231
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,14,self._ctx)

            self.state = 232
            self.exp1()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp1Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp2(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp2Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp2Context,i)


        def OR(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.OR)
            else:
                return self.getToken(MCParser.OR, i)

        def getRuleIndex(self):
            return MCParser.RULE_exp1

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp1" ):
                return visitor.visitExp1(self)
            else:
                return visitor.visitChildren(self)




    def exp1(self):

        localctx = MCParser.Exp1Context(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_exp1)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 234
            self.exp2()
            self.state = 239
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MCParser.OR:
                self.state = 235
                self.match(MCParser.OR)
                self.state = 236
                self.exp2()
                self.state = 241
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp3(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp3Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp3Context,i)


        def AND(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.AND)
            else:
                return self.getToken(MCParser.AND, i)

        def getRuleIndex(self):
            return MCParser.RULE_exp2

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp2" ):
                return visitor.visitExp2(self)
            else:
                return visitor.visitChildren(self)




    def exp2(self):

        localctx = MCParser.Exp2Context(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_exp2)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 242
            self.exp3()
            self.state = 247
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MCParser.AND:
                self.state = 243
                self.match(MCParser.AND)
                self.state = 244
                self.exp3()
                self.state = 249
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp3Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp4(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp4Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp4Context,i)


        def EQ(self):
            return self.getToken(MCParser.EQ, 0)

        def NEQ(self):
            return self.getToken(MCParser.NEQ, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp3

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp3" ):
                return visitor.visitExp3(self)
            else:
                return visitor.visitChildren(self)




    def exp3(self):

        localctx = MCParser.Exp3Context(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_exp3)
        self._la = 0 # Token type
        try:
            self.state = 255
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 250
                self.exp4()
                self.state = 251
                _la = self._input.LA(1)
                if not(_la==MCParser.EQ or _la==MCParser.NEQ):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 252
                self.exp4()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 254
                self.exp4()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp4Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp5(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp5Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp5Context,i)


        def GT(self):
            return self.getToken(MCParser.GT, 0)

        def LT(self):
            return self.getToken(MCParser.LT, 0)

        def GTE(self):
            return self.getToken(MCParser.GTE, 0)

        def LTE(self):
            return self.getToken(MCParser.LTE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_exp4

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp4" ):
                return visitor.visitExp4(self)
            else:
                return visitor.visitChildren(self)




    def exp4(self):

        localctx = MCParser.Exp4Context(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_exp4)
        self._la = 0 # Token type
        try:
            self.state = 262
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 257
                self.exp5()
                self.state = 258
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.LTE) | (1 << MCParser.GTE) | (1 << MCParser.LT) | (1 << MCParser.GT))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 259
                self.exp5()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 261
                self.exp5()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp5Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp6(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp6Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp6Context,i)


        def ADD(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.ADD)
            else:
                return self.getToken(MCParser.ADD, i)

        def SUB(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.SUB)
            else:
                return self.getToken(MCParser.SUB, i)

        def getRuleIndex(self):
            return MCParser.RULE_exp5

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp5" ):
                return visitor.visitExp5(self)
            else:
                return visitor.visitChildren(self)




    def exp5(self):

        localctx = MCParser.Exp5Context(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_exp5)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 264
            self.exp6()
            self.state = 269
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MCParser.ADD or _la==MCParser.SUB:
                self.state = 265
                _la = self._input.LA(1)
                if not(_la==MCParser.ADD or _la==MCParser.SUB):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 266
                self.exp6()
                self.state = 271
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp6Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp7(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Exp7Context)
            else:
                return self.getTypedRuleContext(MCParser.Exp7Context,i)


        def DIV(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.DIV)
            else:
                return self.getToken(MCParser.DIV, i)

        def MUL(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.MUL)
            else:
                return self.getToken(MCParser.MUL, i)

        def MOD(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.MOD)
            else:
                return self.getToken(MCParser.MOD, i)

        def getRuleIndex(self):
            return MCParser.RULE_exp6

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp6" ):
                return visitor.visitExp6(self)
            else:
                return visitor.visitChildren(self)




    def exp6(self):

        localctx = MCParser.Exp6Context(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_exp6)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 272
            self.exp7()
            self.state = 277
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.MUL) | (1 << MCParser.DIV) | (1 << MCParser.MOD))) != 0):
                self.state = 273
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.MUL) | (1 << MCParser.DIV) | (1 << MCParser.MOD))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 274
                self.exp7()
                self.state = 279
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp7Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp7(self):
            return self.getTypedRuleContext(MCParser.Exp7Context,0)


        def SUB(self):
            return self.getToken(MCParser.SUB, 0)

        def NOT(self):
            return self.getToken(MCParser.NOT, 0)

        def arrele(self):
            return self.getTypedRuleContext(MCParser.ArreleContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_exp7

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp7" ):
                return visitor.visitExp7(self)
            else:
                return visitor.visitChildren(self)




    def exp7(self):

        localctx = MCParser.Exp7Context(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_exp7)
        self._la = 0 # Token type
        try:
            self.state = 283
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.SUB, MCParser.NOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 280
                _la = self._input.LA(1)
                if not(_la==MCParser.SUB or _la==MCParser.NOT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 281
                self.exp7()
                pass
            elif token in [MCParser.INTLIT, MCParser.FLOATLIT, MCParser.STRINGLIT, MCParser.BOOLEANLIT, MCParser.ID, MCParser.LB]:
                self.enterOuterAlt(localctx, 2)
                self.state = 282
                self.arrele()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArreleContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp8(self):
            return self.getTypedRuleContext(MCParser.Exp8Context,0)


        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_arrele

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArrele" ):
                return visitor.visitArrele(self)
            else:
                return visitor.visitChildren(self)




    def arrele(self):

        localctx = MCParser.ArreleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_arrele)
        try:
            self.state = 291
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,22,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 285
                self.exp8()
                self.state = 286
                self.match(MCParser.LSB)
                self.state = 287
                self.exp()
                self.state = 288
                self.match(MCParser.RSB)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 290
                self.exp8()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp8Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTLIT(self):
            return self.getToken(MCParser.INTLIT, 0)

        def STRINGLIT(self):
            return self.getToken(MCParser.STRINGLIT, 0)

        def FLOATLIT(self):
            return self.getToken(MCParser.FLOATLIT, 0)

        def BOOLEANLIT(self):
            return self.getToken(MCParser.BOOLEANLIT, 0)

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def subexp(self):
            return self.getTypedRuleContext(MCParser.SubexpContext,0)


        def funcall(self):
            return self.getTypedRuleContext(MCParser.FuncallContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_exp8

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp8" ):
                return visitor.visitExp8(self)
            else:
                return visitor.visitChildren(self)




    def exp8(self):

        localctx = MCParser.Exp8Context(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_exp8)
        try:
            self.state = 300
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,23,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 293
                self.match(MCParser.INTLIT)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 294
                self.match(MCParser.STRINGLIT)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 295
                self.match(MCParser.FLOATLIT)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 296
                self.match(MCParser.BOOLEANLIT)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 297
                self.match(MCParser.ID)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 298
                self.subexp()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 299
                self.funcall()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FuncallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def explist(self):
            return self.getTypedRuleContext(MCParser.ExplistContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_funcall

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncall" ):
                return visitor.visitFuncall(self)
            else:
                return visitor.visitChildren(self)




    def funcall(self):

        localctx = MCParser.FuncallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_funcall)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 302
            self.match(MCParser.ID)
            self.state = 303
            self.match(MCParser.LB)
            self.state = 304
            self.explist()
            self.state = 305
            self.match(MCParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExplistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExpContext)
            else:
                return self.getTypedRuleContext(MCParser.ExpContext,i)


        def CM(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.CM)
            else:
                return self.getToken(MCParser.CM, i)

        def getRuleIndex(self):
            return MCParser.RULE_explist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExplist" ):
                return visitor.visitExplist(self)
            else:
                return visitor.visitChildren(self)




    def explist(self):

        localctx = MCParser.ExplistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_explist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 315
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FLOATLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.BOOLEANLIT) | (1 << MCParser.ID) | (1 << MCParser.LB) | (1 << MCParser.SUB) | (1 << MCParser.NOT))) != 0):
                self.state = 307
                self.exp()
                self.state = 312
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MCParser.CM:
                    self.state = 308
                    self.match(MCParser.CM)
                    self.state = 309
                    self.exp()
                    self.state = 314
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SubexpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def exp(self):
            return self.getTypedRuleContext(MCParser.ExpContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_subexp

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSubexp" ):
                return visitor.visitSubexp(self)
            else:
                return visitor.visitChildren(self)




    def subexp(self):

        localctx = MCParser.SubexpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_subexp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 317
            self.match(MCParser.LB)
            self.state = 318
            self.exp()
            self.state = 319
            self.match(MCParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





