# Generated from main/mc/parser/MC.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\63")
        buf.write("\u018e\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\3\2\6\2m\n\2\r\2\16\2n\3\3\3\3\3\4\3\4")
        buf.write("\5\4u\n\4\3\4\6\4x\n\4\r\4\16\4y\3\5\3\5\3\6\6\6\177\n")
        buf.write("\6\r\6\16\6\u0080\3\6\3\6\7\6\u0085\n\6\f\6\16\6\u0088")
        buf.write("\13\6\3\6\5\6\u008b\n\6\3\6\7\6\u008e\n\6\f\6\16\6\u0091")
        buf.write("\13\6\3\6\3\6\6\6\u0095\n\6\r\6\16\6\u0096\3\6\5\6\u009a")
        buf.write("\n\6\3\6\6\6\u009d\n\6\r\6\16\6\u009e\3\6\5\6\u00a2\n")
        buf.write("\6\3\6\7\6\u00a5\n\6\f\6\16\6\u00a8\13\6\3\6\3\6\5\6\u00ac")
        buf.write("\n\6\3\7\3\7\3\7\3\7\7\7\u00b2\n\7\f\7\16\7\u00b5\13\7")
        buf.write("\3\7\3\7\3\7\3\b\3\b\5\b\u00bc\n\b\3\t\3\t\3\t\3\t\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f")
        buf.write("\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3")
        buf.write("\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3\21")
        buf.write("\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23")
        buf.write("\3\23\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\30")
        buf.write("\6\30\u0113\n\30\r\30\16\30\u0114\3\30\7\30\u0118\n\30")
        buf.write("\f\30\16\30\u011b\13\30\3\31\3\31\3\32\3\32\3\33\3\33")
        buf.write("\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3")
        buf.write("\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3\'\3(\3(\3(\3")
        buf.write(")\3)\3)\3*\3*\3*\3+\3+\3+\3,\3,\3,\3-\3-\3.\3.\3/\3/\3")
        buf.write("\60\3\60\3\60\3\60\7\60\u0155\n\60\f\60\16\60\u0158\13")
        buf.write("\60\3\60\3\60\3\61\3\61\3\61\3\61\7\61\u0160\n\61\f\61")
        buf.write("\16\61\u0163\13\61\3\61\3\61\3\61\3\61\3\61\3\62\6\62")
        buf.write("\u016b\n\62\r\62\16\62\u016c\3\62\3\62\3\63\3\63\3\63")
        buf.write("\3\64\3\64\3\64\3\64\7\64\u0178\n\64\f\64\16\64\u017b")
        buf.write("\13\64\3\64\3\64\3\65\3\65\3\65\3\65\3\65\3\65\7\65\u0185")
        buf.write("\n\65\f\65\16\65\u0188\13\65\3\65\5\65\u018b\n\65\3\65")
        buf.write("\3\65\3\u0161\2\66\3\3\5\2\7\2\t\2\13\4\r\5\17\6\21\7")
        buf.write("\23\b\25\t\27\n\31\13\33\f\35\r\37\16!\17#\20%\21\'\22")
        buf.write(")\23+\24-\25/\26\61\27\63\30\65\31\67\329\33;\34=\35?")
        buf.write("\36A\37C E!G\"I#K$M%O&Q\'S(U)W*Y+[,]-_.a/c\60e\61g\62")
        buf.write("i\63\3\2\n\3\2\62;\4\2GGgg\t\2$$^^ddhhppttvv\6\2\13\f")
        buf.write("\16\17$$^^\5\2C\\aac|\6\2\62;C\\aac|\4\2\f\f\17\17\5\2")
        buf.write("\n\f\16\17\"\"\2\u01a6\2\3\3\2\2\2\2\13\3\2\2\2\2\r\3")
        buf.write("\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2")
        buf.write("\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2")
        buf.write("\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3")
        buf.write("\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61")
        buf.write("\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2")
        buf.write("\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3")
        buf.write("\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M")
        buf.write("\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2")
        buf.write("W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2")
        buf.write("\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2")
        buf.write("\2\3l\3\2\2\2\5p\3\2\2\2\7r\3\2\2\2\t{\3\2\2\2\13\u00ab")
        buf.write("\3\2\2\2\r\u00ad\3\2\2\2\17\u00bb\3\2\2\2\21\u00bd\3\2")
        buf.write("\2\2\23\u00c1\3\2\2\2\25\u00c6\3\2\2\2\27\u00cc\3\2\2")
        buf.write("\2\31\u00d4\3\2\2\2\33\u00db\3\2\2\2\35\u00e1\3\2\2\2")
        buf.write("\37\u00ea\3\2\2\2!\u00ef\3\2\2\2#\u00f3\3\2\2\2%\u00f6")
        buf.write("\3\2\2\2\'\u00fd\3\2\2\2)\u0100\3\2\2\2+\u0106\3\2\2\2")
        buf.write("-\u010b\3\2\2\2/\u0112\3\2\2\2\61\u011c\3\2\2\2\63\u011e")
        buf.write("\3\2\2\2\65\u0120\3\2\2\2\67\u0122\3\2\2\29\u0124\3\2")
        buf.write("\2\2;\u0126\3\2\2\2=\u0128\3\2\2\2?\u012a\3\2\2\2A\u012c")
        buf.write("\3\2\2\2C\u012e\3\2\2\2E\u0130\3\2\2\2G\u0132\3\2\2\2")
        buf.write("I\u0134\3\2\2\2K\u0136\3\2\2\2M\u0138\3\2\2\2O\u013b\3")
        buf.write("\2\2\2Q\u013e\3\2\2\2S\u0141\3\2\2\2U\u0144\3\2\2\2W\u0147")
        buf.write("\3\2\2\2Y\u014a\3\2\2\2[\u014c\3\2\2\2]\u014e\3\2\2\2")
        buf.write("_\u0150\3\2\2\2a\u015b\3\2\2\2c\u016a\3\2\2\2e\u0170\3")
        buf.write("\2\2\2g\u0173\3\2\2\2i\u017e\3\2\2\2km\5\5\3\2lk\3\2\2")
        buf.write("\2mn\3\2\2\2nl\3\2\2\2no\3\2\2\2o\4\3\2\2\2pq\t\2\2\2")
        buf.write("q\6\3\2\2\2rt\t\3\2\2su\7/\2\2ts\3\2\2\2tu\3\2\2\2uw\3")
        buf.write("\2\2\2vx\5\5\3\2wv\3\2\2\2xy\3\2\2\2yw\3\2\2\2yz\3\2\2")
        buf.write("\2z\b\3\2\2\2{|\7\60\2\2|\n\3\2\2\2}\177\5\5\3\2~}\3\2")
        buf.write("\2\2\177\u0080\3\2\2\2\u0080~\3\2\2\2\u0080\u0081\3\2")
        buf.write("\2\2\u0081\u0082\3\2\2\2\u0082\u0086\5\t\5\2\u0083\u0085")
        buf.write("\5\5\3\2\u0084\u0083\3\2\2\2\u0085\u0088\3\2\2\2\u0086")
        buf.write("\u0084\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u008a\3\2\2\2")
        buf.write("\u0088\u0086\3\2\2\2\u0089\u008b\5\7\4\2\u008a\u0089\3")
        buf.write("\2\2\2\u008a\u008b\3\2\2\2\u008b\u00ac\3\2\2\2\u008c\u008e")
        buf.write("\5\5\3\2\u008d\u008c\3\2\2\2\u008e\u0091\3\2\2\2\u008f")
        buf.write("\u008d\3\2\2\2\u008f\u0090\3\2\2\2\u0090\u0092\3\2\2\2")
        buf.write("\u0091\u008f\3\2\2\2\u0092\u0094\5\t\5\2\u0093\u0095\5")
        buf.write("\5\3\2\u0094\u0093\3\2\2\2\u0095\u0096\3\2\2\2\u0096\u0094")
        buf.write("\3\2\2\2\u0096\u0097\3\2\2\2\u0097\u0099\3\2\2\2\u0098")
        buf.write("\u009a\5\7\4\2\u0099\u0098\3\2\2\2\u0099\u009a\3\2\2\2")
        buf.write("\u009a\u00ac\3\2\2\2\u009b\u009d\5\5\3\2\u009c\u009b\3")
        buf.write("\2\2\2\u009d\u009e\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f")
        buf.write("\3\2\2\2\u009f\u00a1\3\2\2\2\u00a0\u00a2\5\t\5\2\u00a1")
        buf.write("\u00a0\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a6\3\2\2\2")
        buf.write("\u00a3\u00a5\5\5\3\2\u00a4\u00a3\3\2\2\2\u00a5\u00a8\3")
        buf.write("\2\2\2\u00a6\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a9")
        buf.write("\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a9\u00aa\5\7\4\2\u00aa")
        buf.write("\u00ac\3\2\2\2\u00ab~\3\2\2\2\u00ab\u008f\3\2\2\2\u00ab")
        buf.write("\u009c\3\2\2\2\u00ac\f\3\2\2\2\u00ad\u00b3\7$\2\2\u00ae")
        buf.write("\u00af\7^\2\2\u00af\u00b2\t\4\2\2\u00b0\u00b2\n\5\2\2")
        buf.write("\u00b1\u00ae\3\2\2\2\u00b1\u00b0\3\2\2\2\u00b2\u00b5\3")
        buf.write("\2\2\2\u00b3\u00b1\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\u00b6")
        buf.write("\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b6\u00b7\7$\2\2\u00b7")
        buf.write("\u00b8\b\7\2\2\u00b8\16\3\2\2\2\u00b9\u00bc\5+\26\2\u00ba")
        buf.write("\u00bc\5-\27\2\u00bb\u00b9\3\2\2\2\u00bb\u00ba\3\2\2\2")
        buf.write("\u00bc\20\3\2\2\2\u00bd\u00be\7k\2\2\u00be\u00bf\7p\2")
        buf.write("\2\u00bf\u00c0\7v\2\2\u00c0\22\3\2\2\2\u00c1\u00c2\7x")
        buf.write("\2\2\u00c2\u00c3\7q\2\2\u00c3\u00c4\7k\2\2\u00c4\u00c5")
        buf.write("\7f\2\2\u00c5\24\3\2\2\2\u00c6\u00c7\7h\2\2\u00c7\u00c8")
        buf.write("\7n\2\2\u00c8\u00c9\7q\2\2\u00c9\u00ca\7c\2\2\u00ca\u00cb")
        buf.write("\7v\2\2\u00cb\26\3\2\2\2\u00cc\u00cd\7d\2\2\u00cd\u00ce")
        buf.write("\7q\2\2\u00ce\u00cf\7q\2\2\u00cf\u00d0\7n\2\2\u00d0\u00d1")
        buf.write("\7g\2\2\u00d1\u00d2\7c\2\2\u00d2\u00d3\7p\2\2\u00d3\30")
        buf.write("\3\2\2\2\u00d4\u00d5\7u\2\2\u00d5\u00d6\7v\2\2\u00d6\u00d7")
        buf.write("\7t\2\2\u00d7\u00d8\7k\2\2\u00d8\u00d9\7p\2\2\u00d9\u00da")
        buf.write("\7i\2\2\u00da\32\3\2\2\2\u00db\u00dc\7d\2\2\u00dc\u00dd")
        buf.write("\7t\2\2\u00dd\u00de\7g\2\2\u00de\u00df\7c\2\2\u00df\u00e0")
        buf.write("\7m\2\2\u00e0\34\3\2\2\2\u00e1\u00e2\7e\2\2\u00e2\u00e3")
        buf.write("\7q\2\2\u00e3\u00e4\7p\2\2\u00e4\u00e5\7v\2\2\u00e5\u00e6")
        buf.write("\7k\2\2\u00e6\u00e7\7p\2\2\u00e7\u00e8\7w\2\2\u00e8\u00e9")
        buf.write("\7g\2\2\u00e9\36\3\2\2\2\u00ea\u00eb\7g\2\2\u00eb\u00ec")
        buf.write("\7n\2\2\u00ec\u00ed\7u\2\2\u00ed\u00ee\7g\2\2\u00ee \3")
        buf.write("\2\2\2\u00ef\u00f0\7h\2\2\u00f0\u00f1\7q\2\2\u00f1\u00f2")
        buf.write("\7t\2\2\u00f2\"\3\2\2\2\u00f3\u00f4\7k\2\2\u00f4\u00f5")
        buf.write("\7h\2\2\u00f5$\3\2\2\2\u00f6\u00f7\7t\2\2\u00f7\u00f8")
        buf.write("\7g\2\2\u00f8\u00f9\7v\2\2\u00f9\u00fa\7w\2\2\u00fa\u00fb")
        buf.write("\7t\2\2\u00fb\u00fc\7p\2\2\u00fc&\3\2\2\2\u00fd\u00fe")
        buf.write("\7f\2\2\u00fe\u00ff\7q\2\2\u00ff(\3\2\2\2\u0100\u0101")
        buf.write("\7y\2\2\u0101\u0102\7j\2\2\u0102\u0103\7k\2\2\u0103\u0104")
        buf.write("\7n\2\2\u0104\u0105\7g\2\2\u0105*\3\2\2\2\u0106\u0107")
        buf.write("\7v\2\2\u0107\u0108\7t\2\2\u0108\u0109\7w\2\2\u0109\u010a")
        buf.write("\7g\2\2\u010a,\3\2\2\2\u010b\u010c\7h\2\2\u010c\u010d")
        buf.write("\7c\2\2\u010d\u010e\7n\2\2\u010e\u010f\7u\2\2\u010f\u0110")
        buf.write("\7g\2\2\u0110.\3\2\2\2\u0111\u0113\t\6\2\2\u0112\u0111")
        buf.write("\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0112\3\2\2\2\u0114")
        buf.write("\u0115\3\2\2\2\u0115\u0119\3\2\2\2\u0116\u0118\t\7\2\2")
        buf.write("\u0117\u0116\3\2\2\2\u0118\u011b\3\2\2\2\u0119\u0117\3")
        buf.write("\2\2\2\u0119\u011a\3\2\2\2\u011a\60\3\2\2\2\u011b\u0119")
        buf.write("\3\2\2\2\u011c\u011d\7*\2\2\u011d\62\3\2\2\2\u011e\u011f")
        buf.write("\7+\2\2\u011f\64\3\2\2\2\u0120\u0121\7}\2\2\u0121\66\3")
        buf.write("\2\2\2\u0122\u0123\7\177\2\2\u01238\3\2\2\2\u0124\u0125")
        buf.write("\7]\2\2\u0125:\3\2\2\2\u0126\u0127\7_\2\2\u0127<\3\2\2")
        buf.write("\2\u0128\u0129\7=\2\2\u0129>\3\2\2\2\u012a\u012b\7.\2")
        buf.write("\2\u012b@\3\2\2\2\u012c\u012d\7-\2\2\u012dB\3\2\2\2\u012e")
        buf.write("\u012f\7/\2\2\u012fD\3\2\2\2\u0130\u0131\7,\2\2\u0131")
        buf.write("F\3\2\2\2\u0132\u0133\7\61\2\2\u0133H\3\2\2\2\u0134\u0135")
        buf.write("\7\'\2\2\u0135J\3\2\2\2\u0136\u0137\7#\2\2\u0137L\3\2")
        buf.write("\2\2\u0138\u0139\7~\2\2\u0139\u013a\7~\2\2\u013aN\3\2")
        buf.write("\2\2\u013b\u013c\7(\2\2\u013c\u013d\7(\2\2\u013dP\3\2")
        buf.write("\2\2\u013e\u013f\7?\2\2\u013f\u0140\7?\2\2\u0140R\3\2")
        buf.write("\2\2\u0141\u0142\7#\2\2\u0142\u0143\7?\2\2\u0143T\3\2")
        buf.write("\2\2\u0144\u0145\7>\2\2\u0145\u0146\7?\2\2\u0146V\3\2")
        buf.write("\2\2\u0147\u0148\7@\2\2\u0148\u0149\7?\2\2\u0149X\3\2")
        buf.write("\2\2\u014a\u014b\7>\2\2\u014bZ\3\2\2\2\u014c\u014d\7@")
        buf.write("\2\2\u014d\\\3\2\2\2\u014e\u014f\7?\2\2\u014f^\3\2\2\2")
        buf.write("\u0150\u0151\7\61\2\2\u0151\u0152\7\61\2\2\u0152\u0156")
        buf.write("\3\2\2\2\u0153\u0155\n\b\2\2\u0154\u0153\3\2\2\2\u0155")
        buf.write("\u0158\3\2\2\2\u0156\u0154\3\2\2\2\u0156\u0157\3\2\2\2")
        buf.write("\u0157\u0159\3\2\2\2\u0158\u0156\3\2\2\2\u0159\u015a\b")
        buf.write("\60\3\2\u015a`\3\2\2\2\u015b\u015c\7\61\2\2\u015c\u015d")
        buf.write("\7,\2\2\u015d\u0161\3\2\2\2\u015e\u0160\13\2\2\2\u015f")
        buf.write("\u015e\3\2\2\2\u0160\u0163\3\2\2\2\u0161\u0162\3\2\2\2")
        buf.write("\u0161\u015f\3\2\2\2\u0162\u0164\3\2\2\2\u0163\u0161\3")
        buf.write("\2\2\2\u0164\u0165\7,\2\2\u0165\u0166\7\61\2\2\u0166\u0167")
        buf.write("\3\2\2\2\u0167\u0168\b\61\3\2\u0168b\3\2\2\2\u0169\u016b")
        buf.write("\t\t\2\2\u016a\u0169\3\2\2\2\u016b\u016c\3\2\2\2\u016c")
        buf.write("\u016a\3\2\2\2\u016c\u016d\3\2\2\2\u016d\u016e\3\2\2\2")
        buf.write("\u016e\u016f\b\62\3\2\u016fd\3\2\2\2\u0170\u0171\13\2")
        buf.write("\2\2\u0171\u0172\b\63\4\2\u0172f\3\2\2\2\u0173\u0179\7")
        buf.write("$\2\2\u0174\u0175\7^\2\2\u0175\u0178\t\4\2\2\u0176\u0178")
        buf.write("\n\5\2\2\u0177\u0174\3\2\2\2\u0177\u0176\3\2\2\2\u0178")
        buf.write("\u017b\3\2\2\2\u0179\u0177\3\2\2\2\u0179\u017a\3\2\2\2")
        buf.write("\u017a\u017c\3\2\2\2\u017b\u0179\3\2\2\2\u017c\u017d\b")
        buf.write("\64\5\2\u017dh\3\2\2\2\u017e\u0186\7$\2\2\u017f\u0180")
        buf.write("\7^\2\2\u0180\u0185\n\4\2\2\u0181\u0185\n\5\2\2\u0182")
        buf.write("\u0183\7^\2\2\u0183\u0185\t\4\2\2\u0184\u017f\3\2\2\2")
        buf.write("\u0184\u0181\3\2\2\2\u0184\u0182\3\2\2\2\u0185\u0188\3")
        buf.write("\2\2\2\u0186\u0184\3\2\2\2\u0186\u0187\3\2\2\2\u0187\u018a")
        buf.write("\3\2\2\2\u0188\u0186\3\2\2\2\u0189\u018b\7$\2\2\u018a")
        buf.write("\u0189\3\2\2\2\u018a\u018b\3\2\2\2\u018b\u018c\3\2\2\2")
        buf.write("\u018c\u018d\b\65\6\2\u018dj\3\2\2\2\35\2nty\u0080\u0086")
        buf.write("\u008a\u008f\u0096\u0099\u009e\u00a1\u00a6\u00ab\u00b1")
        buf.write("\u00b3\u00bb\u0114\u0119\u0156\u0161\u016c\u0177\u0179")
        buf.write("\u0184\u0186\u018a\7\3\7\2\b\2\2\3\63\3\3\64\4\3\65\5")
        return buf.getvalue()


class MCLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    INTLIT = 1
    FLOATLIT = 2
    STRINGLIT = 3
    BOOLEANLIT = 4
    INTTYPE = 5
    VOIDTYPE = 6
    FLOATTYPE = 7
    BOOLTYPE = 8
    STRINGTYPE = 9
    BREAK = 10
    CONT = 11
    ELSE = 12
    FOR = 13
    IF = 14
    RETURN = 15
    DO = 16
    WHILE = 17
    TRUE = 18
    FALSE = 19
    ID = 20
    LB = 21
    RB = 22
    LP = 23
    RP = 24
    LSB = 25
    RSB = 26
    SM = 27
    CM = 28
    ADD = 29
    SUB = 30
    MUL = 31
    DIV = 32
    MOD = 33
    NOT = 34
    OR = 35
    AND = 36
    EQ = 37
    NEQ = 38
    LTE = 39
    GTE = 40
    LT = 41
    GT = 42
    ASSIGN = 43
    LINE_CMT = 44
    BLOCK_CMT = 45
    WS = 46
    ERROR_CHAR = 47
    UNCLOSE_STRING = 48
    ILLEGAL_ESCAPE = 49

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'int'", "'void'", "'float'", "'boolean'", "'string'", "'break'", 
            "'continue'", "'else'", "'for'", "'if'", "'return'", "'do'", 
            "'while'", "'true'", "'false'", "'('", "')'", "'{'", "'}'", 
            "'['", "']'", "';'", "','", "'+'", "'-'", "'*'", "'/'", "'%'", 
            "'!'", "'||'", "'&&'", "'=='", "'!='", "'<='", "'>='", "'<'", 
            "'>'", "'='" ]

    symbolicNames = [ "<INVALID>",
            "INTLIT", "FLOATLIT", "STRINGLIT", "BOOLEANLIT", "INTTYPE", 
            "VOIDTYPE", "FLOATTYPE", "BOOLTYPE", "STRINGTYPE", "BREAK", 
            "CONT", "ELSE", "FOR", "IF", "RETURN", "DO", "WHILE", "TRUE", 
            "FALSE", "ID", "LB", "RB", "LP", "RP", "LSB", "RSB", "SM", "CM", 
            "ADD", "SUB", "MUL", "DIV", "MOD", "NOT", "OR", "AND", "EQ", 
            "NEQ", "LTE", "GTE", "LT", "GT", "ASSIGN", "LINE_CMT", "BLOCK_CMT", 
            "WS", "ERROR_CHAR", "UNCLOSE_STRING", "ILLEGAL_ESCAPE" ]

    ruleNames = [ "INTLIT", "DIGIT", "EXPONENT", "DOT", "FLOATLIT", "STRINGLIT", 
                  "BOOLEANLIT", "INTTYPE", "VOIDTYPE", "FLOATTYPE", "BOOLTYPE", 
                  "STRINGTYPE", "BREAK", "CONT", "ELSE", "FOR", "IF", "RETURN", 
                  "DO", "WHILE", "TRUE", "FALSE", "ID", "LB", "RB", "LP", 
                  "RP", "LSB", "RSB", "SM", "CM", "ADD", "SUB", "MUL", "DIV", 
                  "MOD", "NOT", "OR", "AND", "EQ", "NEQ", "LTE", "GTE", 
                  "LT", "GT", "ASSIGN", "LINE_CMT", "BLOCK_CMT", "WS", "ERROR_CHAR", 
                  "UNCLOSE_STRING", "ILLEGAL_ESCAPE" ]

    grammarFileName = "MC.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def emit(self):
        tk = self.type
        if tk == self.UNCLOSE_STRING:       
            result = super().emit();
            raise UncloseString(result.text);
        elif tk == self.ILLEGAL_ESCAPE:
            result = super().emit();
            raise IllegalEscape(result.text);
        elif tk == self.ERROR_CHAR:
            result = super().emit();
            raise ErrorToken(result.text); 
        else:
            return super().emit();


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[5] = self.STRINGLIT_action 
            actions[49] = self.ERROR_CHAR_action 
            actions[50] = self.UNCLOSE_STRING_action 
            actions[51] = self.ILLEGAL_ESCAPE_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))


    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:

                        start = 1;
                        end = len(self.text) - 1;
                        step = None;
                        self.text = self.text[start:end:step];
                    
     

    def ERROR_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:
             raise ErrorToken(self.text); 
     

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:

                        start = 1
                        end = len(self.text)
                        step = None
                        self.text = self.text[start:end:step]

     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:

                i = 0
                while i <= len(self.text):
                    if self.text[i] == '\\':
                        dest = i + 1
                        if self.text[dest] == '\\':
                            i = i + 2
                            continue
                        if self.text[dest] != 'b' and self.text[dest] != 't' and self.text[dest] != 'n' and self.text[dest] != 'f' and self.text[dest] != 'r' and self.text[dest] != '\"':
                            break
                    i = i + 1
                self.text = self.text[1:dest+1]

     


