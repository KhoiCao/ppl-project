Change current directory to initial/src where there is file run.py
Type: python run.py gen 
Then type: python run.py test LexerSuite
Then type: python run.py test ParserSuite
Then type: python run.py test ASTGenSuite
Then type: python run.py test CheckSuite

My work is in the following files:
- upload/src/main/mc/parser/MC.g4
- upload/src/main/mc/astgen/ASTGeneration.py
- upload/src/main/mc/checker/StaticCheck.py

